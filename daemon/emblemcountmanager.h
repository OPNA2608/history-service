/*
 * Copyright (C) 2022 Ubports Foundation.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 *
 * This file is part of lomiri-history-service.
 *
 * lomiri-history-service is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-history-service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EMBLEMCOUNTMANAGER_H
#define EMBLEMCOUNTMANAGER_H

#include <QDBusInterface>
#include <QDBusServiceWatcher>
#include <QObject>

#include "types.h"

class EmblemCountManager : public QObject
{
    Q_OBJECT
public:
    ~EmblemCountManager();
    explicit EmblemCountManager(QObject *parent = 0);

    void updateCounter(History::EventType type);
    void updateCounters();

private:
    QDBusInterface *getEmblemCounter(History::EventType type);

    QDBusServiceWatcher mWatcher;
};

#endif // EMBLEMCOUNTMANAGER_H
